import React from "react";
import Info from "./components/info";
import Form from "./components/form";
import Weather from "./components/weather";



const API_KEY = "f961192c9968a368935d2bd839a82ff7";


class App extends React.Component {

    state = {
      city: undefined,
      temperature: undefined,
      country: undefined,
      sunrise: undefined,
      sunset: undefined,
      error: undefined
    }


    gettingWeather = async ( event ) => {
      event.preventDefault();
      const CITY_NAME = event.target.elements.city.value;
      

      if( CITY_NAME ) {
        const WEATHER_URL = await fetch (`http://api.openweathermap.org/data/2.5/weather?q=${CITY_NAME}&appid=${API_KEY}&units=metric`)
        const data = await WEATHER_URL.json();
        
        const sunrise = data.sys.sunrise;
        const sunset = data.sys.sunset;

        const createSunriseTime = new Date();
        const createSunsetTime = new Date();

        createSunriseTime.setTime( sunrise * 1000 );
        createSunsetTime.setTime( sunset * 1000 );

        const convertSunriseTime = createSunriseTime.getHours() + ":" + createSunriseTime.getMinutes();
        const convertSunsetTime = createSunsetTime.getHours() + ":" + createSunsetTime.getMinutes();

        this.setState({
          city: data.name,
          temperature: data.main.temp,
          country: data.sys.country,
          sunrise: convertSunriseTime,
          sunset: convertSunsetTime,
          error: undefined
        });

      } else{
        this.setState({
          city: undefined,
          temperature: undefined,
          country: undefined,
          sunrise: undefined,
          sunset: undefined,
          error: "Вы не выбрали город!"
        });
      }
    }

      render() {
        return(
          <div className = "wrapper">
            <div className = "main">
              <div className = "container">
                  <div className = "row">
                    <div className = "col-sm-5 info">
                      <Info />
                    </div>
                    <div className = "col-sm-7 form">
                      <Form weatherMethod = { this.gettingWeather } />
                      <Weather
                      city = { this.state.city }
                      temperature = { this.state.temperature }
                      country = { this.state.country }
                      sunrise = { this.state.sunrise }
                      sunset = { this.state.sunset }
                      error = { this.state.error }
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
        );
      }

}

export default App;