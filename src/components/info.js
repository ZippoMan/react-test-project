import React from "react"

const Info = () => {
    return (
        <div>
            <h2>Погодный сервис</h2>
            <p>Узнайте погоду в вашем городе!</p>
        </div>
    );
}

export default Info