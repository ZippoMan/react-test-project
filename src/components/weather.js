import React from "react"

const Weather = ( props ) => {

    return (
        <div>
            { props.city && 
                <div className = "infoWeath">
                    <p>Город: {props.city}, {props.country}</p>
                    <p>Температура: {props.temperature}°</p>
                    <p>Время восхода солнца: {props.sunrise}</p>
                    <p>Время захода солнца: {props.sunset}</p>
                </div>
            }
            <p className = "error">{props.error}</p>
        </div>
    );
}


export default Weather